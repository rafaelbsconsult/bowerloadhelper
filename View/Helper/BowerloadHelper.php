<?php
/**
 *  Português: Helper criado para importar arquivos do bower
 *  English: Helper creatad for import bower files
 * Incluir no header do layout
 *
 * include it in your var $helpers = array('Bowerload')
 */
class BowerloadHelper extends AppHelper {
    //Used helpers
    public $helpers = array('Js', 'Html');
    public $patchBower = APP.WEBROOT_DIR.DS.'bower_components'.DS;
    public $folderBower = DS.'bower_components'.DS;
    private $dependences = array();
    
       public function __construct(View $view, $settings = array()) {
        $this->defaultDependence();
        parent::__construct($view, $settings);
        
    }


    public function addDependence($type,$file)
    {
      
       
        $this->dependences[] =['type'=>$type,'folder' => $file];
    }

    public function search($type)
    {
        switch ($type) {
            case 'angular':
            $this->addDependence('js','angular/angular');
        }
        $this->renderscript(); 
    }

    public function renderscript()
    {
        foreach ($this->dependences as $type => $file) {

                if(is_file($this->patchBower.$file['folder'].'.'.$file['type'])){

                    if($file['type'] == 'js'){
                      echo $this->js($file['folder']);   
                    }
                    else{
                        echo $this->css($file['folder']);
                    }
                   
                }
        }

    }


    public function beforeRender($viewFile)
    {

        //dependências básicas de desenvolvimento
        $this->renderscript();    
        parent::beforeRender($viewFile);

    }


    private function defaultDependence()
    {
        $this->dependences[] =['type'=>'js','folder' => 'jquery/dist/jquery'];
        $this->dependences[] =['type'=>'js','folder' => 'jquery-ui/jquery-ui'];
        $this->dependences[] =['type'=>'css','folder' => 'jquery-ui/themes/smoothness/jquery-ui'];
        $this->dependences[] =['type'=>'js','folder' => 'bootstrap/dist/js/bootstrap'];  
        $this->dependences[] =['type'=>'css','folder' => 'bootstrap/dist/css/bootstrap']; 

    }

    //return all javascript collection
   private  function js($folder){
    //Verificar se o vc está solicitando todos os arquivos da pasta
        $checkIsAll = strstr($folder, '*');

        if($checkIsAll !=false){
             $folder = str_replace('*', '', $folder);

                $files = $this->walker($this->patchBower.$folder.DS, null, 'js');
                $files = array_merge($files, $this->walker($this->patchBower.$folder.DS, null, 'js'));

                $files = array_unique($files);
                $files = $this->strip_slash($files);
                 // debug($files);exit();
                foreach($files as $file){
                echo  $this->Html->script($this->folderBower.$folder.$file);
            }
        } else {
            echo $this->Html->script($this->folderBower.$folder);
        }


    }
    
    //Pegar todos os arquivos
function css($folder){
    //Verificar se o vc está solicitando todos os arquivos da pasta

    $checkIsAll = strstr($folder, '*');
    if($checkIsAll != false){
        $folder = str_replace('*', '', $folder);
        $files = $this->walker($this->folderBower.DS.$folder, null, 'css');
        $files = array_merge($files, $this->walker($this->folderBower.DS.$folder, null, 'css'));
        $files = array_merge($files, $this->walker(CSS, null, 'css'));
        $files = array_unique($files);
        foreach($files as $file){
            echo  $this->Html->css($this->folderBower.$file);
        }
    }else {
        echo $this->Html->css($this->folderBower.$folder);
    }
}

  
    private function strip_slash($array){
        foreach($array as $key=>$value){
            if(substr($value, 0, 1) == '/'){
            $array[$key] = substr($value, 1);
            }
        }
        return $array;
    }

    private function walker( $from, $web_dir = null, $extension){
        $dirs = array();
        $files = array();
        if(!is_dir($from))
        {
            return $files;
        }
        
        $d = dir($from);
        while (false !== ($entry = $d->read())) {
            if( $entry == '.' || $entry == '..' || substr($entry, 0, 1) == '.' || $entry == 'views'){continue;}
            if(is_dir($from.DS.$entry)){
                $dirs[] = $entry;
            }
            if(is_file($from.DS.$entry) && (substr($entry, -1*strlen($extension)) == $extension)){
                if($web_dir == null){
                    $files[] = $entry;
                } else {
                    $files[] = $web_dir.DS.$entry;
                }
            }
        }
        $d->close();
        sort($files);
        foreach($dirs as $entry){
            $files = array_merge($files, $this->walker( $from.DS.$entry, $web_dir.'/'.$entry, $extension));
        }
        return $files;
    }
}
